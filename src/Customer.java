import java.util.ArrayList;

public class Customer implements IDisplay, Comparable<Customer> {
	private int customerID;
	private String firstName;
	private String lastName;
	private String emailId;
	private ArrayList<Bill> billList = new ArrayList<Bill>();
	private double totalAmount;

	public Customer() {
	}

	public Customer(int customerID, String firstName, String lastName, String emailId, ArrayList<Bill> billList,
			double totalAmount) {
		this.customerID = customerID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.billList = billList;
		this.totalAmount = totalAmount;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public ArrayList<Bill> getBillList() {

		return billList;
	}

	public void setBillList(ArrayList<Bill> billList) {
		this.billList = billList;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public void display() {
		System.out.println("Customer Id: " + customerID + "\n" + "Customer Full Name: " + firstName + " " + lastName
				+ "\n" + "Customer Email ID: " + emailId + "\n");
	}

	@Override
	public int compareTo(Customer customer2) {
		// This function defines the rules for comparison

		if (this.customerID == customer2.getCustomerID()) {
			return 0;
		} else if ((this.customerID > customer2.getCustomerID())) {
			return 1;
		} else {
			// this.customerID < customer2.getCustomerID()
			return -1;
		}

	}

	public static boolean isMailValid(String email) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return email.matches(regex);
	}
}
