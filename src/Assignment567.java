import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Assignment567 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//creating bills
		Internet internetBill1 = new Internet();
		internetBill1.setBillId(100);
		internetBill1.setBillDate(LocalDate.of(2019, 07, 29));
		internetBill1.setBillType("Internet");
		internetBill1.setBillAmount(250.99);
		internetBill1.setProvideName("Fido");
		internetBill1.setInternetUsage("900.35 MB");

		Internet internetBill2 = new Internet();
		internetBill2.setBillId(101);
		internetBill2.setBillDate(LocalDate.of(2019, 06, 28));
		internetBill2.setBillType("Internet");
		internetBill2.setBillAmount(220.35);
		internetBill2.setProvideName("Rogers");
		internetBill2.setInternetUsage("500 GB");

		Hydro hydroBill1 = new Hydro();
		hydroBill1.setBillId(050);
		hydroBill1.setBillDate(LocalDate.of(2019, 8, 30));
		hydroBill1.setBillType("Hydro");
		hydroBill1.setBillAmount(125.50);
		hydroBill1.setAgencyName("Enercare");
		hydroBill1.setUnitConsumed("430 Units");

		Hydro hydroBill2 = new Hydro();
		hydroBill2.setBillId(051);
		hydroBill2.setBillDate(LocalDate.of(2019, 05, 26));
		hydroBill2.setBillType("Hydro");
		hydroBill2.setBillAmount(125.50);
		hydroBill2.setAgencyName("Hydro Canada");
		hydroBill2.setUnitConsumed("320 Units");

		Mobile mobileBill1 = new Mobile();
		mobileBill1.setBillId(200);
		mobileBill1.setBillDate(LocalDate.of(2019, 06, 03));
		mobileBill1.setBillType("Mobile");
		mobileBill1.setBillAmount(199.99);
		mobileBill1.setManufactureName("Bell");
		mobileBill1.setMinuteUsed(90);
		mobileBill1.setInternetUsage(339.50);
		mobileBill1.setMobileNumber("4168285485");
		mobileBill1.setPlanName("Sargent Peppers");

		//list of bills
		ArrayList<Bill> listBillc1 = new ArrayList<Bill>();
		listBillc1.add(internetBill1);
		listBillc1.add(hydroBill1);

		ArrayList<Bill> listBillc2 = new ArrayList<Bill>();
		listBillc2.add(internetBill2);
		listBillc2.add(hydroBill2);

		// checking phone number - if different of 10 digits,
		// Show error message
		if (mobileBill1.getMobileNumber().length() != 10) {
			System.out.println("No Mobile bill - Phone number error!!! ");
		} else {
			listBillc2.add(mobileBill1);
		}

		ArrayList<Bill> listBillc3 = new ArrayList<Bill>();

		//creating customers
		Customer customer1 = new Customer(2019005, "John", "Lennon", "j.lennon@gmail.com", listBillc1,
				getTotalAmount(listBillc1));
		Customer customer2 = new Customer(2019001, "George", "Harrison", "g.harrison@gmail.com", listBillc2,
				getTotalAmount(listBillc2));
		Customer customer3 = new Customer(2019003, "Ringo", "Starr", "r.starr@gmail.com", listBillc3,
				getTotalAmount(listBillc3));

		// email validation - if email is valid, add customer on customer list
		// else, print error message
		ArrayList<Customer> customer = new ArrayList<Customer>();
		if (Customer.isMailValid(customer1.getEmailId())) {
			customer.add(customer1);
		} else {
			System.out.println("Bills not added for Customer 1 - Invalid email");
		}
		if (Customer.isMailValid(customer2.getEmailId())) {
			customer.add(customer2);
		} else {
			System.out.println("Bills not added for Customer 2 - Invalid email");
		}
		if (Customer.isMailValid(customer3.getEmailId())) {
			customer.add(customer3);
		} else {
			System.out.println("Bills not added for Customer 3 - Invalid email");
		}

		// Expect a input from the user to search for a customer by ID
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the ID of the customer: ");
		int customerId = keyboard.nextInt();
		
		//search for a customer by ID
		if (getCustomerById(customerId, customer) != null) {
			System.out.println(getCustomerById(customerId, customer));
		} else
			System.out.println("******************************************\n" + "	    Customer not found!\n"
					+ "******************************************\n\n"
					+ "****************BILL LIST*****************\n");

		// 1a. print the customer information
		Collections.sort(customer);
		for (Customer c : customer) {

			// 1b. print the customer information
			c.display();

			// 2. print the customer's bills
			System.out.println("      ---- Bill Information ----");
			System.out.println("******************************************");

			// 2a. Get the list of the customer bills
			ArrayList<Bill> bills = c.getBillList();

			// 2b. Check if the list is empty

			if (bills.size() == 0) {
				System.out.println("~~~NOTE : This Customer has no bills");
				System.out.println("******************************************\n");
			} else {

				// 2c. sort the list of bills according to total amount
				// Collections.sort() is a default Java function.
				// It automatically sorts an ArrayList!
				// So , use the function, and your arrayList is automatically sorted!
				Collections.sort(bills);

				// 2d. print out the bill
				for (Bill b : bills) {
					b.display();
				}

				// 4. Print out the total sum of bills
				double total = c.getTotalAmount();
				System.out.printf("	   Total amount: $%.2f", total);
				System.out.println("\n******************************************\n");
			}
		}
	}

	//method to get the total amount of bills 
	private static double getTotalAmount(ArrayList<Bill> _listBill) {
		double totalBillAmount = 0.0;
		for (Bill bill : _listBill) {
			totalBillAmount += bill.getTotalBillAmount();
		}
		return totalBillAmount;
	}

	// method to get customer by id
	private static String getCustomerById(int id, ArrayList<Customer> customerList) {
		for (Customer customer : customerList) {

			if (customer.getCustomerID() == id) {
				String sCustomer = "************We found it****************\n" + "*Customer Id: "
						+ customer.getCustomerID() + "\n" + "*Customer Full Name: " + customer.getFirstName() + " "
						+ customer.getLastName() + "\n" + "*Customer Email ID: " + customer.getEmailId()
						+ "\n***************************************\n\n" + "*************BILL LIST*****************\n";
				return sCustomer;
			}

		}
		return null;
	}
}
