
public class Hydro extends Bill implements IDisplay {
	private String agencyName;
	private String unitConsumed;
	
	public Hydro() {
	}

	public Hydro(String agencyName, String unitConsumed) {
		this.agencyName = agencyName;
		this.unitConsumed = unitConsumed;
		
	}

	public double getBillAmount() {
		return super.getTotalBillAmount();
	}

	public void setBillAmount(double billAmount) {
		super.setTotalBillAmount(billAmount);
	}

	public String getAgencyName() {
		return agencyName;
	}
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}
	public String getUnitConsumed() {
		return unitConsumed;
	}
	public void setUnitConsumed(String unitConsumed) {
		this.unitConsumed = unitConsumed;
	}
	
	@Override
	public void display() {
		
		String output = 
				   "	Company Name: "+getAgencyName() + "\n"+
				   "	Unit Consumed: "+getUnitConsumed()+"\n"+
				   "******************************************";
		super.display();
		System.out.println(output);
	}
}
