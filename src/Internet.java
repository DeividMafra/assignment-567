public class Internet extends Bill implements IDisplay{
	private String provideName;
	private String internetUsage;
	//private double billAmount;

	
	public Internet() {
	}

	public Internet(String provideName, String internetUsage, double billAmount) {
		
		this.provideName = provideName;
		this.internetUsage = internetUsage;
		
		// set the bill amount
		super.setTotalBillAmount(billAmount);
		
		//this.billAmount = billAmount;
	}

	public String getProvideName() {
		return provideName;
	}
	public void setProvideName(String provideName) {
		this.provideName = provideName;
	}
	public String getInternetUsage() {
		return internetUsage;
	}
	public void setInternetUsage(String internetUsage) {
		this.internetUsage = internetUsage;
	}
	
	public double getBillAmount() {
		return super.getTotalBillAmount();
	}

	public void setBillAmount(double billAmount) {
		super.setTotalBillAmount(billAmount);
	}
	
	@Override
	public void display() {
		// gets the amount of the bill from the parent class (Bill)
		super.display();
		String output =
				   "	Provider Name: "+getProvideName() +"\n"+
				   "	Internet Usage: "+getInternetUsage()+"\n"+
				   "******************************************";
		
		System.out.println(output);
	}
}
