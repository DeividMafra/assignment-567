
public class Mobile extends Bill implements IDisplay{
	private String manufactureName; 
	private String planName;
	private String mobileNumber;
	private double internetUsage;
	private int minuteUsed;
	
	public Mobile() {
	}

	public Mobile(String manufactureName, String planName, String mobileNumber, double internetUsage, int minuteUsed) {
		this.manufactureName = manufactureName;
		this.planName = planName;
		this.mobileNumber = mobileNumber;
		this.internetUsage = internetUsage;
		this.minuteUsed = minuteUsed;
		
	}

	public double getBillAmount() {
		return super.getTotalBillAmount();
	}

	public void setBillAmount(double billAmount) {
		super.setTotalBillAmount(billAmount);
	}
	
	public String getManufactureName() {
		return manufactureName;
	}
	public void setManufactureName(String manufactureName) {
		this.manufactureName = manufactureName;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public double getInternetUsage() {
		return internetUsage;
	}
	public void setInternetUsage(double internetUsage) {
		this.internetUsage = internetUsage;
	}
	public int getMinuteUsed() {
		return minuteUsed;
	}
	public void setMinuteUsed(int minuteUsed) {
		this.minuteUsed = minuteUsed;
	}	
	
	
	
	public String toString() {
		
		return  "Bill Id: "+getBillId()+ "\n"+
				"Bill Date: "+ getBillDate()+"\n"+
				"Bill Type: "+ getBillType()+ "\n"+
				"Bill Amount: "+getBillAmount()+"\n"+
				"Manufacturer Name: "+manufactureName+"\n"+
				"Plane Name: "+planName +"\n"+
				"Mobile Number: "+mobileNumber+"\n"+
				"Internet Usage: "+internetUsage+"\n"+
				"Minutes Usage: "+minuteUsed+"\n"+
				"******************************************";
		
	}
	@Override
	public void display() {
		// TODO Auto-generated method stub
		
		String output = 
				
			
				"	Manufacturer Name: "+manufactureName+"\n"+
				"	Plane Name: "+planName +"\n"+
				"	Mobile Number: "+mobileNumber+"\n"+
				"	Internet Usage: "+internetUsage+"\n"+
				"	Minutes Usage: "+minuteUsed+"\n"+
				"******************************************";
		super.display();
		System.out.println(output);
	}
	
}
