import java.time.LocalDate;

public class Bill implements IDisplay, Comparable<Bill> {
	
	private int billId;
	private LocalDate billDate;
	private String billType;
	private double totalBillAmount;
	
	public Bill() {
	}
	public Bill(int billId, LocalDate billDate, String billType, double totalBillAmount) {
		this.billId = billId;
		this.billDate = billDate;
		this.billType = billType;
		this.totalBillAmount = totalBillAmount;
	}
	public Bill(Internet internetBill, Hydro hydroBill) {
		
	}
	public Bill(Internet internetBill, Hydro hydroBill, Mobile mobileBill) {
		
	}
	
	public int getBillId() {
		return billId;
	}
	public void setBillId(int billId) {
		this.billId = billId;
	}
	public LocalDate getBillDate() {
		return billDate;
	}
	public void setBillDate(LocalDate localDate) {
		this.billDate = localDate;
	}
	public String getBillType() {
		return billType;
	}
	public void setBillType(String billType) {
		this.billType = billType;
	}
	public double getTotalBillAmount() {
		return totalBillAmount;
	}
	public void setTotalBillAmount(double totalBillAmount) {
		this.totalBillAmount = totalBillAmount;
	}
	
	@Override
	public void display() {
		// TODO Auto-generated method stub
		String output = 
				"	Bill Id: "+getBillId()+ "\n"+
				"	Bill Date: "+ getBillDate()+"\n"+
			    "	Bill Type: "+ getBillType();
		System.out.println(output);
		System.out.printf("	Bill Amount: $%.2f", getTotalBillAmount());
		System.out.println();
	}
	
	@Override
	public int compareTo(Bill bill2) {
		// This function defines the rules for comparison
		// According to rules below, we are comparing bills based n their total amount
		// But, you can change this to whatever you want (compare based on id, first name, provider type, etc etc)
		// Just change the property
		// According to java:
		//		A == B, return 0
		//		A > B, return 1
		//  	A < B, return -1
		
		if (this.totalBillAmount == bill2.getTotalBillAmount()) {
			return 0;
		}
		else if (this.totalBillAmount > bill2.getTotalBillAmount()) {
			return 1;
		}
		else  {
			// this bill is < bill2.getTotalAmount()
			return -1;
		}
	
	}
	
}
